# spork

Can do multiple things.

But right now it can only do one thing. Stop and start proxies. Try:

```
node index.js -r proxy_name
```

## Misc.

examples of running command line things:

http://krasimirtsonev.com/blog/article/Nodejs-managing-child-processes-starting-stopping-exec-spawn

http://stackoverflow.com/questions/20643470/execute-a-command-line-binary-with-node-js
