#! /usr/bin/env node

var chalk = require('chalk');
var exec = require('child_process').exec;

var proxies = require('./lib/proxies');

var argv = require('minimist')(process.argv.slice(2));

var commands = [];

if (argv.r) {
  var r = argv.r;
  commands = proxies(r);
}

commands.forEach(function(command) {
  exec(command, function(error, stdout, stderr) {
    console.log(chalk.blue(stdout));
    if (stderr) {
      console.error(chalk.red('stderr: ' + stderr));
    }
    if (error !== null) {
        console.error(chalk.red('exec error: ' + error));
    }
  });
});
