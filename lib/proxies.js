var chalk = require('chalk');

module.exports = function (proxy) {

  var commands = [];

  var proxies = [
    'squirrel',
    'pws_authenticated_user',
    'scio_identified_records',
    'scio_unidentified_records',
    'identity_search'
  ];

  var stopStart = function (proxy) {
    return [
      'sudo stop nodejs_ws_proxy_' + proxy,
      'sudo start nodejs_ws_proxy_' + proxy
    ];
  };

  if (proxy === true) {
    console.log(chalk.red('\nno proxy listed!\n'));
    console.log('possible proxies...');
    proxies.forEach(function(proxy){
      console.log(proxy);
    });
  } else {
    commands = stopStart(proxy);
  }

  return commands;

};
